#include <iostream>


int najveci(int niz[5], int& n) {
    int max;
    for(int i = 1; i < n; ++i){
        if(niz[0] < niz[i])
            max = niz[i];
    }
    return max;
}

int najmanji(int niz[5], int& n) {
    for(int i = 0; i < n; i++){
        if(niz[0] > niz[i])
            niz[0] = niz[i];
    }
    return niz[0];
}

int main() {
    int niz[5]={1,2,3,4,5};
    int n = 5;

    int v = najveci(niz, n);
    int m = najmanji(niz,n);
    std::cout << "Najveci je: " << v << std::endl;
    std::cout << "Najmanji je: " << m << std::endl;
}